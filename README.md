# Assignment 1.4 Pokerhands

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)


## Pokerhands

Generates a hand of poker when upon user prompt. Tells you what hand it is according to these rules: https://en.wikipedia.org/wiki/List_of_poker_hands

## Requirements
gcc

## Usage
Compile and run it. Then press enter. 

## Maintainers
Sam Nassiri
