#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <algorithm>
#include <cstdlib>

struct Card                 //create card structure
{
  char faceValue;
  std::string suit;
  int index;
};

bool sortOrder(Card A, Card B);         //declearing variables for checking hands
bool fiveCheck;
bool royalCheck;
bool suitCheck;
bool straightflushCheck;
bool fourCheck;
bool houseCheck;
bool flushCheck;
bool straightCheck;
bool threeCheck;
bool twopairCheck;
bool pairCheck;

bool fiveKind(std::vector<Card> Hand);          //declariing functions for checking hands
bool sameSuit(std::vector<Card> Hand);
bool checkStraight(std::vector<Card> Hand);
bool RoyalFlush (std::vector<Card> Hand);
bool fourKind (std::vector<Card> Hand);
bool threeKind (std::vector<Card> Hand);
bool twoPair (std::vector<Card> Hand);
bool onePair (std::vector<Card> Hand);

int main() {
    std::vector<Card> deck;                     //initialize our deck vector
    std::vector<Card> Hand;                     // initalize our Hand vector
    
  deck.push_back({'2', "Clubs",2});
  deck.push_back({'3', "Clubs",3});
  deck.push_back({'4', "Clubs",4});
  deck.push_back({'5', "Clubs",5});
  deck.push_back({'6', "Clubs",6});
  deck.push_back({'7', "Clubs",7});
  deck.push_back({'8', "Clubs",8});
  deck.push_back({'9', "Clubs",9});
  deck.push_back({'T', "Clubs",10});
  deck.push_back({'J', "Clubs",11});
  deck.push_back({'Q', "Clubs",12});
  deck.push_back({'K', "Clubs",13});
  deck.push_back({'A', "Clubs",14});

  deck.push_back({'2', "Diamonds",2});
  deck.push_back({'3', "Diamonds",3});
  deck.push_back({'4', "Diamonds",4});
  deck.push_back({'5', "Diamonds",5});
  deck.push_back({'6', "Diamonds",6});
  deck.push_back({'7', "Diamonds",7});
  deck.push_back({'8', "Diamonds",8});
  deck.push_back({'9', "Diamonds",9});
  deck.push_back({'T', "Diamonds",10});
  deck.push_back({'J', "Diamonds",11});
  deck.push_back({'Q', "Diamonds",12});
  deck.push_back({'K', "Diamonds",13});
  deck.push_back({'A', "Diamonds",14});

  deck.push_back({'2', "Hearts",2});
  deck.push_back({'3', "Hearts",3});
  deck.push_back({'4', "Hearts",4});
  deck.push_back({'5', "Hearts",5});
  deck.push_back({'6', "Hearts",6});
  deck.push_back({'7', "Hearts",7});
  deck.push_back({'8', "Hearts",8});
  deck.push_back({'9', "Hearts",9});
  deck.push_back({'T', "Hearts",10});
  deck.push_back({'J', "Hearts",11});
  deck.push_back({'Q', "Hearts",12});
  deck.push_back({'K', "Hearts",13});
  deck.push_back({'A', "Hearts",14});

  deck.push_back({'2', "Spades",2});
  deck.push_back({'3', "Spades",3});
  deck.push_back({'4', "Spades",4});
  deck.push_back({'5', "Spades",5});
  deck.push_back({'6', "Spades",6});
  deck.push_back({'7', "Spades",7});
  deck.push_back({'8', "Spades",8});
  deck.push_back({'9', "Spades",9});
  deck.push_back({'T', "Spades",10});
  deck.push_back({'J', "Spades",11});
  deck.push_back({'Q', "Spades",12});
  deck.push_back({'K', "Spades",13});
  deck.push_back({'A', "Spades",14});


std::vector<Card> deck2;                        //creates deck2
deck2 = deck;

    while(true){

        std::cout<<"Press enter for cards ";
        std::cin.ignore();
        srand(time(NULL));
        
        for (size_t i = 0; i < 5; i++)                      //draws 5 cards
        {
            int decknumber = rand() % 2;
            int cardnumber;
            if( decknumber == 1){
                cardnumber = rand() % deck.size();
                Hand.push_back(deck[cardnumber]);
                deck.erase(deck.begin() + cardnumber);
            }else{
                cardnumber = rand() % deck2.size();
                Hand.push_back(deck2[cardnumber]);
                deck2.erase(deck2.begin() + cardnumber);
            }
        }
        std::sort(Hand.begin(), Hand.end(), sortOrder);             //sorts in ascending order
        for (size_t i = 0; i < 5; i++)
        {
            std::cout<<Hand[i].faceValue<<" of "<<Hand[i].suit<<std::endl;
        }
        fiveCheck=fiveKind(Hand);
        royalCheck=RoyalFlush(Hand);
        suitCheck=sameSuit(Hand);
        straightCheck=checkStraight(Hand);
        fourCheck=fourKind(Hand);
        houseCheck;
        flushCheck;
        threeCheck=threeKind(Hand);
        twopairCheck=twoPair(Hand);
        pairCheck=onePair(Hand);

        if (fiveCheck==1){
            std::cout<<"You got five of a kind "<<std::endl;
            continue;                                       // get a new hand
        }
        else if (royalCheck==1 && suitCheck==1) {
            std::cout<<"You got royal flush "<<std::endl;
            continue;                                       // get a new hand
        }
        else if (straightCheck==1 && suitCheck==1){
            std::cout<<"You got a straight flush "<<std::endl;
            continue;                                       // get a new hand
        }
        else if (fourCheck==1){
            std::cout<<"You got a four of a kind "<<std::endl;
            continue;                                       // get a new hand
        }
        else if (threeCheck==1 && pairCheck==1){
            std::cout<<"You got a full house "<<std::endl;
            continue;                                       // get a new hand
        }
        else if (suitCheck==1){
            std::cout<<"You got a flush "<<std::endl;
            continue;                                       // get a new hand
        }
        else if (straightCheck==1){
            std::cout<<"You got a straight "<<std::endl;
            continue;                                       // get a new hand
        }
        else if (threeCheck==1){
            std::cout<<"You got a three of a kind "<<std::endl;
            continue;                                   // get a new hand
        }
        else if (twopairCheck==1){
            std::cout<<"You got a two-pair "<<std::endl;
            continue;                                           // get a new hand
        }
        else if (pairCheck==1){
            std::cout<<"You got a pair "<<std::endl;
            continue;                                           // get a new hand
        }   
        else {
            std::cout<<"You got a a high card "<<Hand[4].faceValue<<std::endl;
            continue;                                           // get a new hand
        }
    }
}

bool sortOrder(Card A, Card B)                  //sorts cards
    {
    if (A.index<B.index)
    {
        return 1;
    }
    else 
    {
        return 0;
    }
    }

bool fiveKind(std::vector<Card> Hand){              //checks for five of a kind
    int fiver = 0;
    for (size_t i=1; i<5; i++)
    {   if (Hand[i-1].index==Hand[i].index)
        {
            fiver=fiver+1;
        }
    
    }
    if (fiver==4) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}
bool sameSuit(std::vector<Card> Hand){                  //checks whether they are all of the same suit
    int suiter=0;
    for (size_t i=1; i<5; i++)
    {   if (Hand[i-1].suit==Hand[i].suit)
        {
            suiter=suiter+1;
        }
    
    }
    if (suiter==4) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

bool checkStraight(std::vector<Card> Hand){                 // checks for a straight
    int straight=0;
    for (size_t i=1; i<5; i++) 
    {
        if ((Hand[i].index)-(Hand[i-1].index)==1)
        {
            straight=straight+1;
        }
    }
    if (straight==4) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

bool RoyalFlush (std::vector<Card> Hand) {              //checks for a royal flush
    if (Hand[0].index==10 && Hand[1].index==11 && Hand[2].index==12 && Hand[3].index==13 && Hand[4].index==14)
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

bool fourKind (std::vector<Card> Hand) {            //checks four of a kind
    for (size_t i=0; i<5; i++)
    {  int four=0;
       for (size_t j=i+1; j<5; j++) 
        {
            if (Hand[i].index==Hand[j].index)
            {
            four=four+1;            
            }
        }
        if (four==3)
            {
        return 1;
            }
    }
        return 0;
}
bool threeKind (std::vector<Card> Hand) {                   //checks three of a kind
    for (size_t i=0; i<5; i++) 
    {
        int tree=0;
        for (size_t j=i+1; j<5; j++) 
        {
            if (Hand[i].index==Hand[j].index) 
            {
            tree=tree+1;           
            }
        }
        if (tree==2) 
        {
        return 1;
        }
    }
    return 0;
}
bool twoPair (std::vector<Card> Hand) {             //checks for a 2 pair
    int twopair=0;
    for (size_t i=0; i<5; i++)
    {   for (size_t j=i+1; j<5; j++) 
        {
            if (Hand[i].index==Hand[j].index)
            {
            twopair=twopair+1;            
            }
        }
    }
    if (twopair==2)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
bool onePair (std::vector<Card> Hand) {                 //checks for a pair
    int onepair=0;
    for (size_t i=0; i<5; i++)
    {   for (size_t j=i+1; j<5; j++) 
        {
            if (Hand[i].index==Hand[j].index)
            {
            onepair=onepair+1;            
            }
        }
    }
    if (onepair==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
